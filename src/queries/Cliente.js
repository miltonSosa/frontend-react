import gql from 'graphql-tag';

export const ClienteQuery = gql `{
    getClientes{
        id
        nombre
        apellido
        email
      }
}`;